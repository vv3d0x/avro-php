<?php

/**
 * Copyright 2024 Joyride GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace AvroTest\Serialization\Message\BinaryEncoding;

use Avro\AvroException;
use Avro\Model\Schema\Fixed;
use Avro\Model\Schema\NamespacedName;
use Avro\Serialization\Message\BinaryEncoding\FixedEncoding;
use Avro\Serialization\Message\BinaryEncoding\StringByteReader;
use PHPUnit\Framework\TestCase;

class FixedEncodingTest extends TestCase
{
  /**
   * @dataProvider arrayData
   *
   * @throws AvroException
   */
  public function testDecode(string $data, Fixed $schema, string $expected): void
  {
    $decoded = FixedEncoding::decode($schema, new StringByteReader($data));

    $this->assertEquals($expected, $decoded);
  }

  /**
   * @dataProvider arrayData
   */
  public function testEncode(string $expected, Fixed $schema, string $value): void
  {
    $this->assertEquals($expected, FixedEncoding::encode($schema, $value));
  }

  public static function arrayData(): array
  {
    $fixedSchema = Fixed::named(NamespacedName::fromValue('test'), 4);

    return [
      ['ABCD', $fixedSchema, 'ABCD'],
      ["AB\0\0", $fixedSchema, "AB\0\0"],
    ];
  }
}
