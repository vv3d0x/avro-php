<?php

/**
 * Copyright 2024 Joyride GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Avro\Validation;

use Avro\Model\Schema\Primitive;

final class PrimitiveValidating
{
  public const MIN_INT_VALUE = (1 << 31) * -1;

  public const MAX_INT_VALUE = (1 << 31) - 1;

  public const MIN_LONG_VALUE = PHP_INT_MIN;

  public const MAX_LONG_VALUE = PHP_INT_MAX;

  /**
   * @throws \UnexpectedValueException
   */
  public static function isValid(mixed $value, Primitive $schema): bool
  {
    return match ($schema->getType()) {
      Primitive::TYPE_NULL => null === $value,
      Primitive::TYPE_BOOLEAN => \is_bool($value),
      Primitive::TYPE_INT => \is_int($value)
        && $value >= self::MIN_INT_VALUE
        && $value <= self::MAX_INT_VALUE,
      Primitive::TYPE_LONG => \is_int($value)
        && $value >= self::MIN_LONG_VALUE
        && $value <= self::MAX_LONG_VALUE,
      Primitive::TYPE_FLOAT, Primitive::TYPE_DOUBLE => \is_int($value) || \is_float($value),
      Primitive::TYPE_STRING, Primitive::TYPE_BYTES => \is_string($value),
      default => throw new \UnexpectedValueException('Cannot validate unknown primitive type'),
    };
  }
}
