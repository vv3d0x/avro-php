<?php

/**
 * Copyright 2024 Joyride GmbH.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare(strict_types=1);

namespace Avro\Serialization\Message\BinaryEncoding;

use Avro\Model\Schema\Primitive;
use Avro\Serialization\Message\BinaryEncoding\Util\VarLengthZigZagEncoding;
use LogicException;
use RuntimeException;

class PrimitiveEncoding
{
  /**
   * Encode a Primitive according to its type
   *
   */
  public static function encode(Primitive $schema, mixed $value): string
  {
    $type = $schema->getType();

    return match ($type) {
      Primitive::TYPE_NULL => '',
      Primitive::TYPE_BOOLEAN => self::encodeBoolean($value),
      Primitive::TYPE_STRING => self::encodeString($value),
      Primitive::TYPE_BYTES => self::encodeBytes($value),
      Primitive::TYPE_INT, Primitive::TYPE_LONG => self::encodeLongOrInt($value),
      Primitive::TYPE_FLOAT => self::encodeFloat($value),
      Primitive::TYPE_DOUBLE => self::encodeDouble($value),
      default => throw new LogicException(\sprintf('Unknown primitive type "%s"', $type)),
    };
  }

  /**
   * Decode a Primitive according to its type
   *
   * @throws ReadError
   */
  public static function decode(Primitive $primitive, ByteReader $byteReader): mixed
  {
    return match ($primitive->getType()) {
      Primitive::TYPE_NULL => null,
      Primitive::TYPE_BOOLEAN => self::decodeBoolean($byteReader),
      Primitive::TYPE_STRING => self::decodeString($byteReader),
      Primitive::TYPE_BYTES => self::decodeBytes($byteReader),
      Primitive::TYPE_INT, Primitive::TYPE_LONG => self::decodeLongOrInt($byteReader),
      Primitive::TYPE_FLOAT => self::decodeFloat($byteReader),
      Primitive::TYPE_DOUBLE => self::decodeDouble($byteReader),
      default => throw new LogicException(\sprintf('Unknown primitive type "%s"', $primitive->getType())),
    };
  }

  /**
   * Encode a string which is a UTF-8 sequence prefixed with its byte-length
   *
   * Since PHP does not differentiate between byte-strings and UTF-8 strings
   * this is the same as a number of bytes.
   *
   */
  public static function encodeString(string $value): string
  {
    return self::encodeBytes($value);
  }

  /**
   * Decode a string which is a UTF-8 sequence prefixed with its byte-length
   *
   * Since PHP does not differentiate between byte-strings and UTF-8 strings
   * this is the same as a number of bytes.
   *
   * @throws ReadError
   */
  public static function decodeString(ByteReader $byteReader): string
  {
    return self::decodeBytes($byteReader);
  }

  /**
   * Encode a byte-sequence which is the raw bytes prefixed with the number of bytes
   *
   */
  public static function encodeBytes(string $bytes): string
  {
    return self::encodeLongOrInt(\strlen($bytes)) . $bytes;
  }

  /**
   * Decode a byte-sequence which is the raw bytes prefixed with the number of bytes
   *
   * @throws ReadError
   */
  public static function decodeBytes(ByteReader $byteReader): string
  {
    $numBytes = self::decodeLongOrInt($byteReader);

    return $byteReader->read($numBytes);
  }

  /**
   * Encode a long or int which is encoded with variable length zig-zag-encoding
   *
   */
  public static function encodeLongOrInt(int $value): string
  {
    return VarLengthZigZagEncoding::encode($value);
  }

  /**
   * Decode a long or int which is encoded with variable length zig-zag-encoding
   *
   * @throws ReadError
   */
  public static function decodeLongOrInt(ByteReader $byteReader): int
  {
    return VarLengthZigZagEncoding::decode($byteReader);
  }

  /**
   * Encode a float (IEEE 754 single precision)
   *
   * This method assumes PHP is running on the IEEE 754 floating-point
   * implementation, which should be almost always true. If it is not,
   * you already know.
   *
   */
  public static function encodeFloat(float $value): string
  {
    return \pack('f', $value);
  }

  /**
   * Decode a float (IEEE 754 single precision)
   *
   * This method assumes PHP is running on the IEEE 754 floating-point
   * implementation, which should be almost always true. If it is not,
   * you already know.
   *
   * @throws ReadError
   */
  public static function decodeFloat(ByteReader $byteReader): float
  {
    $bytes = $byteReader->read(4);
    $unpacked = \unpack('f', $bytes);

    if (false === $unpacked) {
      throw new RuntimeException('Failed to unpack float');
    }

    return (float) $unpacked[1];
  }

  /**
   * Encode a float (IEEE 754 double precision)
   *
   * This method assumes PHP is running on the IEEE 754 floating-point
   * implementation, which should be almost always true. If it is not,
   * you already know.
   *
   */
  public static function encodeDouble(float $value): string
  {
    return \pack('d', $value);
  }

  /**
   * Decode a float (IEEE 754 double precision)
   *
   * This method assumes PHP is running on the IEEE 754 floating-point
   * implementation, which should be almost always true. If it is not,
   * you already know.
   *
   * @throws ReadError
   */
  public static function decodeDouble(ByteReader $byteReader): float
  {
    $bytes = $byteReader->read(8);

    $unpacked = \unpack('d', $bytes);

    if (false === $unpacked) {
      throw new RuntimeException('Failed to unpack double');
    }

    return (float) $unpacked[1];
  }

  public static function encodeBoolean(bool $value): string
  {
    return $value ? "\1" : "\0";
  }

  /**
   * @throws ReadError
   */
  public static function decodeBoolean(ByteReader $byteReader): bool
  {
    return match ($byteReader->read(1)) {
      "\x0" => false,
      "\x1" => true,
      default => throw new ReadError('Invalid byte for boolean type'),
    };
  }
}
