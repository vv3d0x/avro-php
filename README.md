# Avro PHP Library

## Introduction

This is a non official Avro php implementation.

If you want to know more about the reason why this library has emerged, please have a look
at [this discussion on the official mailing list](https://sematext.com/opensee/m/F2svI1rXdGP1scOkm1?subj=Modern+PHP+support).

## Installation

This project is distributed as a composer package, available
on [packagist.org](https://packagist.org/packages/jaumo/avro).

Run the following command to add it to your project:

```bash
$ composer require jaumo/avro
```

## Examples

Check the `examples/` directory to understand how to use this library.

## License

Read the [LICENSE](./LICENSE) file to know more about terms and conditions for use, reproduction, and distribution.
